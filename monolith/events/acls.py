import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers).json()
    try:
        picture = {
            "picture_url": r["photos"][0]["url"]
        }
    except IndexError:
        return None
    return picture

def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    lat_lon = requests.get(url).json()
    print(lat_lon)
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat_lon[0]["lat"]}&lon={lat_lon[0]["lon"]}&appid={OPEN_WEATHER_API_KEY}"
    r = requests.get(url).json()
    weather = {
        "temp": r["main"]["temp"],
        "description": r["weather"][0]["description"]
    }
    return weather
